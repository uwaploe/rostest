// Rostest is a command interpreter for running one or more ROS rotators.
package main

import (
	"bufio"
	"context"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"os/signal"
	"runtime"
	"strconv"
	"strings"
	"syscall"
	"time"

	ros "bitbucket.org/mfkenney/go-ros"
	prompt "github.com/c-bata/go-prompt"
	ansi "github.com/k0kubun/go-ansi"
	shellwords "github.com/mattn/go-shellwords"
	"github.com/tarm/serial"
)

const USAGE = `Usage: rostest [options] name=device [name=device ...]

Control one or more ROS rotators. Each name refers to a rotation
axis and device is the serial device used to communicate with
the rotator.
`

var Version = "dev"
var BuildDate = "unknown"

const PROMPT = "ROS> "

var (
	showvers = flag.Bool("version", false,
		"Show program version information and exit")
	baudRate = flag.Int("baud", 9600, "Serial baud rate for Rotators")
	brakeVal = flag.Int("brake", 89, "Rotator brake setting")
	maxVel   = flag.Float64("vmax", 1, "Maximum rotator velocity in deg/s")
	accel    = flag.Float64("accel", 2, "Rotator acceleration in deg/s^2")
)

type completer struct {
	cmds map[string]Commander
}

type executor struct {
	cmds map[string]Commander
}

func (c *completer) complete(d prompt.Document) []prompt.Suggest {
	bc := d.TextBeforeCursor()
	if bc == "" {
		return nil
	}

	args := strings.Split(bc, " ")

	var s []prompt.Suggest
	cmd, ok := c.cmds[args[0]]
	if !ok {
		if len(args) == 1 {
			// number of commands + help
			s = make([]prompt.Suggest, len(c.cmds)+1)
			s = append(s,
				prompt.Suggest{Text: "help", Description: "show help message"})
			for name, cmd := range c.cmds {
				s = append(s,
					prompt.Suggest{Text: name, Description: cmd.Synopsis()})
			}
		}
	} else {
		s = cmd.Completion()
	}

	return prompt.FilterHasPrefix(s, d.GetWordBeforeCursor(), true)
}

func (e *executor) execute(l string) bool {
	if l == "quit" || l == "exit" {
		return true
	}

	// Ignore spaces
	if len(strings.TrimSpace(l)) == 0 {
		return false
	}

	parts, err := shellwords.Parse(l)
	if err != nil {
		fmt.Fprintf(os.Stderr, "ERROR: %v\n", err)
		return false
	}

	if parts[0] == "help" {
		showHelp(os.Stdout, e.cmds)
		return false
	}

	cmd, ok := e.cmds[parts[0]]
	if !ok {
		fmt.Fprintf(os.Stderr, "Unknown command: %q\n", parts[0])
		return false
	}

	var args []string
	if len(parts) != 1 {
		args = parts[1:]
	}

	if err := cmd.Validate(args); err != nil {
		fmt.Fprintf(os.Stderr, "ERROR: %v\n", err)
		fmt.Fprint(os.Stderr, cmd.Help())
		return false
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM, syscall.SIGHUP)
	defer signal.Stop(sigs)

	// Catch signals
	go func() {
		<-sigs
		cancel()
	}()

	result, err := cmd.Run(ctx, args)
	if err != nil {
		fmt.Fprintf(os.Stderr, "ERROR: %v\n", err)
	} else {
		fmt.Fprintln(os.Stdout, result)
	}

	return false
}

func showHelp(w io.Writer, cmds map[string]Commander) {
	var maxLen int
	// slice of [name, synopsis]
	text := make([][]string, 0, len(cmds))
	for name, cmd := range cmds {
		text = append(text, []string{name, cmd.Synopsis()})
		if len(name) > maxLen {
			maxLen = len(name)
		}
	}

	text = append(text, []string{"quit", "exit program"})
	if maxLen < 4 {
		maxLen = 4
	}

	var cmdText string
	for name, cmd := range cmds {
		cmdText += fmt.Sprintf("  %-"+strconv.Itoa(maxLen)+"s    %s\n",
			name, cmd.Synopsis())
	}

	fmt.Fprintf(w, "Available commands:\n%s", cmdText)
}

func repl(devs map[string]ros.Rotator) {
	cmds := map[string]Commander{
		"where":  makeWhereCommand(devs),
		"limits": makeLimitsCommand(devs),
		"move":   makeMoveCommand(devs, false),
		"moveto": makeMoveCommand(devs, true),
		"stop":   makeStopCommand(devs),
		"speed":  makeValCommand(devs, "speed", "Vmax"),
		"accel":  makeValCommand(devs, "accel", "Accel"),
		"brake":  makeBrakeCommand(devs),
	}

	executor := &executor{cmds: cmds}
	completer := &completer{cmds: cmds}
	defer ansi.CursorShow()

	var input string
	for {
		ansi.CursorShow()
		if runtime.GOOS == "windows" {
			// on windows, use a plain input prompt,
			// as the ANSI codes from the prompt package
			// are not well supported
			fmt.Print(PROMPT)
			scanner := bufio.NewScanner(os.Stdin)
			scanner.Scan()
			input = scanner.Text()
		} else {
			input = prompt.Input(PROMPT, completer.complete)
		}
		ansi.CursorHide()
		if executor.execute(input) {
			break
		}
	}
}

func main() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, USAGE)
		flag.PrintDefaults()
	}

	flag.Parse()
	if *showvers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	args := flag.Args()
	if len(args) < 1 {
		flag.Usage()
		os.Exit(1)
	}

	m := ros.Motion{
		Vmax:  float32(*maxVel),
		Accel: float32(*accel),
		Brake: *brakeVal,
	}

	devs := make(map[string]ros.Rotator)
	for _, arg := range args {
		parts := strings.Split(arg, "=")
		if len(parts) != 2 {
			log.Fatal("Bad argument; %q", arg)
		}
		cfg := &serial.Config{
			Name:        parts[1],
			Baud:        *baudRate,
			ReadTimeout: time.Second * 5,
		}
		p, err := serial.OpenPort(cfg)
		if err != nil {
			log.Fatal("Cannot open %q; %v", parts[1], err)
		}
		devs[parts[0]] = ros.NewRotator(p, "A")
		log.Printf("%s: %#v", parts[0], devs[parts[0]].Settings())
		devs[parts[0]].SetMotion(m)
	}

	repl(devs)
}
