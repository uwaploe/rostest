module bitbucket.org/uwaploe/rostest

require (
	bitbucket.org/mfkenney/go-ros v0.3.1
	bitbucket.org/uwaplmlf2/go-shell v0.6.0
	github.com/c-bata/go-prompt v0.2.3
	github.com/k0kubun/go-ansi v0.0.0-20180517002512-3bf9e2903213
	github.com/mattn/go-runewidth v0.0.4 // indirect
	github.com/mattn/go-shellwords v1.0.5
	github.com/pkg/errors v0.8.0
	github.com/pkg/term v0.0.0-20190109203006-aa71e9d9e942 // indirect
	github.com/tarm/serial v0.0.0-20180830185346-98f6abe2eb07
	golang.org/x/sys v0.0.0-20190609082536-301114b31cce // indirect
)
