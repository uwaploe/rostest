package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"reflect"
	"strconv"
	"strings"
	"sync"
	"time"

	ros "bitbucket.org/mfkenney/go-ros"
	prompt "github.com/c-bata/go-prompt"
	"github.com/pkg/errors"
)

type Commander interface {
	Help() string
	Synopsis() string
	Validate(args []string) error
	Completion() []prompt.Suggest
	Run(ctx context.Context, args []string) (string, error)
}

type whereCommand struct {
	devs map[string]ros.Rotator
	axes []string
}

func makeWhereCommand(r map[string]ros.Rotator) Commander {
	c := whereCommand{devs: r}
	c.axes = make([]string, 0, len(r))
	for k, _ := range r {
		c.axes = append(c.axes, k)
	}
	return &c
}

func (c *whereCommand) Help() string {
	return "usage: where"
}

func (c *whereCommand) Synopsis() string {
	return "return the current angle of all rotators"
}

func (c *whereCommand) Validate(args []string) error {
	return nil
}

func (c *whereCommand) Completion() []prompt.Suggest {
	return nil
}

func (c *whereCommand) Run(ctx context.Context, args []string) (string, error) {
	output := make([]string, 0, len(c.axes))
	for _, name := range c.axes {
		angle, err := c.devs[name].Position()
		if err != nil {
			output = append(output, fmt.Sprintf("%s: %v", name, err))
		} else {
			output = append(output, fmt.Sprintf("%s=%.1f", name, angle))
		}
	}

	return strings.Join(output, "\n"), nil
}

type dataSample struct {
	axis  string
	angle float32
}

func motionMonitor(ctx context.Context, channels map[string]<-chan float32) <-chan dataSample {
	var wg sync.WaitGroup

	output := make(chan dataSample, len(channels))
	wg.Add(len(channels))
	mon := func(axis string, ch <-chan float32) {
		defer wg.Done()
		for pos := range ch {
			select {
			case <-ctx.Done():
				log.Printf("Interrupted: %v", ctx.Err())
				return
			case output <- dataSample{axis: axis, angle: pos}:
			}
		}
	}

	for k, v := range channels {
		go mon(k, v)
	}

	go func() {
		wg.Wait()
		close(output)
	}()

	return output
}

type moveCommand struct {
	devs     map[string]ros.Rotator
	axes     []string
	fs       *flag.FlagSet
	target   map[string]*float64
	useSteps bool
	absMove  bool
}

func makeMoveCommand(r map[string]ros.Rotator, absMove bool) Commander {
	c := moveCommand{devs: r, absMove: absMove}
	c.axes = make([]string, 0, len(r))
	for k, _ := range r {
		c.axes = append(c.axes, k)
	}
	c.fs = flag.NewFlagSet("move", flag.ContinueOnError)
	c.target = make(map[string]*float64)
	for _, name := range c.axes {
		c.target[name] = c.fs.Float64(name, 0,
			fmt.Sprintf("angle for %s rotator", name))
	}
	c.fs.BoolVar(&c.useSteps, "steps", false, "if true use step based motion")
	return &c
}

func (c *moveCommand) Help() string {
	var b strings.Builder
	if c.absMove {
		b.WriteString("usage: moveto [options]\n")
	} else {
		b.WriteString("usage: move [options]\n")
	}
	c.fs.SetOutput(&b)
	c.fs.PrintDefaults()
	return b.String()
}

func (c *moveCommand) Synopsis() string {
	if c.absMove {
		return "adjust one or more rotators to an absolute angle"
	}
	return "adjust one or more rotators by a relative angle"
}

func (c *moveCommand) Validate(args []string) error {
	// Set options to their default values
	c.useSteps = false
	for _, v := range c.target {
		*v = 0
	}
	return c.fs.Parse(args)
}

func (c *moveCommand) Completion() []prompt.Suggest {
	s := make([]prompt.Suggest, 0, len(c.axes)+2)
	for _, name := range c.axes {
		s = append(s, prompt.Suggest{
			Text:        "--" + name + "=",
			Description: "angle for " + name + " rotator",
		})
	}
	s = append(s, prompt.Suggest{
		Text:        "--steps",
		Description: "use high-precision steps based motion"})
	return s
}

func (c *moveCommand) Run(ctx context.Context, args []string) (string, error) {
	channels := make(map[string]<-chan float32)
	for axis, v := range c.target {
		if *v == 0 {
			continue
		}

		if c.absMove {
			angle, err := c.devs[axis].Position()
			if err != nil {
				fmt.Printf("%s error: %v\n", axis, err)
				continue
			}
			*v = *v - float64(angle)
		}

		ch, err := c.devs[axis].Move(ctx, float32(*v), c.useSteps, time.Second)
		if err != nil {
			fmt.Printf("%s error: %v\n", axis, err)
			continue
		}
		channels[axis] = ch
	}

	n := len(channels)
	buf := make(map[string]float32)
	for s := range motionMonitor(ctx, channels) {
		buf[s.axis] = s.angle
		if len(buf) == n {
			for k, v := range buf {
				fmt.Printf("%s=%.1f ", k, v)
			}
			fmt.Println()
			buf = make(map[string]float32)
		}
	}
	return "", nil
}

type valCommand struct {
	devs            map[string]ros.Rotator
	axes            []string
	fs              *flag.FlagSet
	values          map[string]*float64
	name, fieldName string
}

func makeValCommand(r map[string]ros.Rotator, name, field string) Commander {
	c := valCommand{devs: r, name: name, fieldName: field}
	c.axes = make([]string, 0, len(r))
	for k, _ := range r {
		c.axes = append(c.axes, k)
	}
	c.fs = flag.NewFlagSet(c.name, flag.ContinueOnError)
	c.values = make(map[string]*float64)
	for _, name := range c.axes {
		c.values[name] = c.fs.Float64(name, 0,
			fmt.Sprintf("set %s of %s rotator", c.name, name))
	}
	return &c
}

func (c *valCommand) Help() string {
	var b strings.Builder
	b.WriteString(fmt.Sprintf("usage: %s [options]\n", c.name))
	c.fs.SetOutput(&b)
	c.fs.PrintDefaults()
	return b.String()
}

func (c *valCommand) Synopsis() string {
	return fmt.Sprintf("get/set the %s value of one or more rotators", c.name)
}

func (c *valCommand) Validate(args []string) error {
	for _, v := range c.values {
		*v = 0
	}
	return c.fs.Parse(args)
}

func (c *valCommand) Completion() []prompt.Suggest {
	s := make([]prompt.Suggest, 0, len(c.axes))
	for _, name := range c.axes {
		s = append(s, prompt.Suggest{
			Text:        "--" + name + "=",
			Description: c.name + " value for " + name + " rotator",
		})
	}
	return s
}

func (c *valCommand) Run(ctx context.Context, args []string) (string, error) {
	output := make([]string, 0, len(c.axes))
	if len(args) == 0 {
		for _, name := range c.axes {
			m, err := c.devs[name].Motion()
			val := reflect.ValueOf(m).FieldByName(c.fieldName)
			if err != nil {
				output = append(output, fmt.Sprintf("%s: %v", name, err))
			} else {
				output = append(output, fmt.Sprintf("%s=%.1f", name, val.Float()))
			}
		}
	} else {
		for axis, v := range c.values {
			if *v == 0 {
				continue
			}
			m, err := c.devs[axis].Motion()
			if err != nil {
				output = append(output, fmt.Sprintf("%s: %v", axis, err))
				continue
			}
			val := reflect.ValueOf(m).FieldByName(c.fieldName)
			val.SetFloat(*v)

			err = c.devs[axis].SetMotion(m)
			if err != nil {
				output = append(output, fmt.Sprintf("%s: %v", axis, err))
			}
		}
	}
	return strings.Join(output, "\n"), nil
}

type brakeCommand struct {
	devs   map[string]ros.Rotator
	axes   []string
	fs     *flag.FlagSet
	values map[string]*int64
}

func makeBrakeCommand(r map[string]ros.Rotator) Commander {
	c := brakeCommand{devs: r}
	c.axes = make([]string, 0, len(r))
	for k, _ := range r {
		c.axes = append(c.axes, k)
	}
	c.fs = flag.NewFlagSet("brake", flag.ContinueOnError)
	c.values = make(map[string]*int64)
	for _, name := range c.axes {
		c.values[name] = c.fs.Int64(name, 0,
			fmt.Sprintf("set brake value of %s rotator", name))
	}
	return &c
}

func (c *brakeCommand) Help() string {
	var b strings.Builder
	b.WriteString("usage: brake [options]\n")
	c.fs.SetOutput(&b)
	c.fs.PrintDefaults()
	return b.String()
}

func (c *brakeCommand) Synopsis() string {
	return fmt.Sprintf("get/set the brake value of one or more rotators")
}

func (c *brakeCommand) Validate(args []string) error {
	for _, v := range c.values {
		*v = 0
	}
	return c.fs.Parse(args)
}

func (c *brakeCommand) Completion() []prompt.Suggest {
	s := make([]prompt.Suggest, 0, len(c.axes))
	for _, name := range c.axes {
		s = append(s, prompt.Suggest{Text: "--" + name + "="})
	}
	return s
}

func (c *brakeCommand) Run(ctx context.Context, args []string) (string, error) {
	output := make([]string, 0, len(c.axes))
	if len(args) == 0 {
		for _, name := range c.axes {
			m, err := c.devs[name].Motion()
			if err != nil {
				output = append(output, fmt.Sprintf("%s: %v", name, err))
			} else {
				output = append(output, fmt.Sprintf("%s=%d", name, m.Brake))
			}
		}
	} else {
		for axis, v := range c.values {
			if *v == 0 {
				continue
			}
			m, err := c.devs[axis].Motion()
			if err != nil {
				output = append(output, fmt.Sprintf("%s: %v", axis, err))
				continue
			}
			m.Brake = int(*v)
			err = c.devs[axis].SetMotion(m)
			if err != nil {
				output = append(output, fmt.Sprintf("%s: %v", axis, err))
			}
		}
	}
	return strings.Join(output, "\n"), nil
}

type limitsCommand struct {
	devs map[string]ros.Rotator
}

func makeLimitsCommand(r map[string]ros.Rotator) Commander {
	c := limitsCommand{devs: r}
	return &c
}

func (c *limitsCommand) Help() string {
	return "usage: limits axis [ccw cw]"
}

func (c *limitsCommand) Synopsis() string {
	return fmt.Sprintf("get/set the limits value of a rotator")
}

func (c *limitsCommand) Validate(args []string) error {
	n := len(args)
	if n != 1 && n != 3 {
		return errors.New("Missing arguments")
	}

	if _, ok := c.devs[args[0]]; !ok {
		return fmt.Errorf("Invalid axis name: %q", args[0])
	}

	return nil
}

func (c *limitsCommand) Completion() []prompt.Suggest {
	s := make([]prompt.Suggest, 0, len(c.devs))
	for name, _ := range c.devs {
		s = append(s, prompt.Suggest{Text: name})
	}
	return s
}

func (c *limitsCommand) Run(ctx context.Context, args []string) (string, error) {
	axis := args[0]
	if len(args) == 1 {
		return fmt.Sprintf("%s", c.devs[axis].Limits()), nil
	}
	var l ros.Limits
	x, err := strconv.ParseFloat(args[1], 32)
	if err != nil {
		return "", errors.Wrap(err, "ccw value")
	}
	l.Ccw = float32(x)

	x, err = strconv.ParseFloat(args[2], 32)
	if err != nil {
		return "", errors.Wrap(err, "cw value")
	}
	l.Cw = float32(x)

	return "", c.devs[axis].SetLimits(l)
}

type stopCommand struct {
	devs map[string]ros.Rotator
}

func makeStopCommand(r map[string]ros.Rotator) Commander {
	c := stopCommand{devs: r}
	return &c
}

func (c *stopCommand) Help() string {
	return "usage: stop"
}

func (c *stopCommand) Synopsis() string {
	return fmt.Sprintf("stop all rotators")
}

func (c *stopCommand) Validate(args []string) error {
	return nil
}

func (c *stopCommand) Completion() []prompt.Suggest {
	return nil
}

func (c *stopCommand) Run(ctx context.Context, args []string) (string, error) {
	var b strings.Builder
	for axis, dev := range c.devs {
		m, err := dev.Motion()
		if err != nil {
			fmt.Fprintf(&b, "%s: %v\n", axis, err)
			continue
		}
		err = dev.Stop(m.Brake, false)
		if err != nil {
			fmt.Fprintf(&b, "%s: %v\n", axis, err)
		}
	}
	return b.String(), nil
}
